import React from 'react'

const Header = () => {
  return (
    <div className='header'>
      <h1 style={{color:"white"}}>Task Assigner</h1>
    </div>
  )
}

export default Header
