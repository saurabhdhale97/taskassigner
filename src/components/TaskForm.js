import React, { useState, useContext, useEffect } from 'react'
import { TaskListContext } from '../contexts/TaskListContext'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';

const TaskForm = () => {
  const { addTask, clearList, editTask, editItem } = useContext(TaskListContext)
  const [pr_name, setName] = useState('')
  const [date, setDate] = useState('')
  const [title, setTitle] = useState('')
  const [age, setAge] = useState('')
  const [gender, setGender] = useState('')
  const [status, setStatus] = useState('Active')
  const [hobby, setHobby] = useState({
    sports: false,
    reading: false,
    music: false,
  });

  const handleSubmit = e => {
    e.preventDefault()
    if (!editItem) {
      addTask(pr_name,date,title,age,gender,status,hobby)
      setTitle('')
      setAge('')
      setGender('')
      setName('')
      setDate('')
      setStatus('Active')
      setName('')
      setHobby({
        sports: false,
        reading: false,
        music: false,
      })

    } else {
      editTask(pr_name,date,title,age,gender,status,hobby, editItem.id)
    }
  }

  const handleChange = e => {
    setTitle(e.target.value)
  }
  const handleChangeAge = e => {
    // if (e.target.value < 18){
    //   setAge(18)
    // } else if (e.target.value > 55){
    //   setAge(55)
    // } else{
    //   setAge(e.target.value)
    // }
    setAge(e.target.value)
  }
  const handleChangeGender = e => {
    setGender(e.target.value)
    console.log(gender);
  }
  const handleChangeName = e => {
    setName(e.target.value)
  }
  const handleChangeDate = e => {
    setDate(e.target.value)
  }
  const handleChangeStatus = e => {
    setStatus(e.target.value)
  }
  const handleChangeHobby = (event) => {
    setHobby({ ...hobby, [event.target.name]: event.target.checked });
  };
  console.log(hobby);
  useEffect(() => {
    if (editItem) {
      setTitle(editItem.title)
      setAge(editItem.age)
      setGender(editItem.gender)
      setName(editItem.pr_name)
      setDate(editItem.date)
      setStatus(editItem.status)
      setHobby(editItem.hobby)
      console.log(editItem)
    } else {
      setTitle('')
      setAge('')
      setGender('')
      setStatus('Active')
      setName('')
      setHobby({
        sports: false,
        reading: false,
        music: false,
      })
    }
  }, [editItem])

  return (
    <form onSubmit={handleSubmit} className="form">
      
      <input
        pattern="[a-zA-Z]{0,15}\s?([a-zA-Z]{0,9})?"
        type="text"
        placeholder="Name"
        value={pr_name}
        onChange={handleChangeName}
        required
        className="task-input"
      />
      <input
        type="text"
        placeholder="Add Task..."
        value={title}
        onChange={handleChange}
        required
        className="task-input"
      />
      <input
        min="18"
        max="55"
        type="number"
        placeholder="age between 18 to 55 Only..."
        value={age}
        onChange={handleChangeAge}
        required
        className="task-input"
      />
      <input
        type="date"
        placeholder="Date."
        value={date}
        onChange={handleChangeDate}
        required
        className="task-input"
      />
      {/* <input
      type="text"
      placeholder="Date."
      value={status}
      onChange={handleChangeStatus}
      required
      className="task-input"
    /> */}
      <Select style = {{marginTop : "10px"}}
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={status}
        onChange={handleChangeStatus}
      >
        <MenuItem value={"Active"}>Active</MenuItem>
        <MenuItem value={"Inactive"}>Inactive</MenuItem>
      </Select>
      <RadioGroup row aria-label="gender" name="gender1" value={gender} onChange={handleChangeGender}>
        <FormControlLabel value="female" control={<Radio />} label="Female" />
        <FormControlLabel value="male" control={<Radio />} label="Male" />
      </RadioGroup>
      <FormGroup aria-label="position" row>
        <FormControlLabel
            control={<Checkbox checked={hobby.sports} onChange={handleChangeHobby} name="sports" />}
            label="Sports"
          />
          <FormControlLabel
            control={<Checkbox checked={hobby.reading} onChange={handleChangeHobby} name="reading" />}
            label="Reading"
          />
          <FormControlLabel
            control={<Checkbox checked={hobby.music} onChange={handleChangeHobby} name="music" />}
            label="Music"
          />
      </FormGroup>
      
      <div className="buttons">
        <button type="submit" className="btn add-task-btn">
          {editItem ? 'Edit Task' : 'Add Task'}
        </button>
        <button className="btn clear-btn" onClick={clearList}>
          Clear
        </button>
      </div>
    </form>
  )
}

export default TaskForm
